# Widget in React 16.4

The testimonials carousel is a important section of a website. It's a marketing tool, whose message is "this product/service is proved,and it's good. Trust us!".

This is a responsive and minimal client testimonials box, that can be easily integrated into React app.  I considered the use case where the user wants to see more feedbacks, so there is a button that links to a modal page with more testimonials with a nice CSS3 transition.

It is used 2 UX patterns to implement widget:

* Carousel with CSSTransition
* Masonry layout

## Technologies:

* CSSTransitionGroup for animation
* Multiple components
* Controlled states via Redux
* Tests with jest, enzyme, redux-mock-store

![img testimonials carousel](./Peek2%202018-08-06%2020-04.gif)

