import { SEE_ALL, CLOSE } from './constants'

export function ScreenShow(visible) {
  return { type: SEE_ALL, payload: visible  }
}
export function ScreenHide(visible) {
  return { type: CLOSE, payload: visible  }
}