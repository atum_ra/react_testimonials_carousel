import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { store } from './Store/store';

import Carousel from "./Components/Carousel";
import Screen from "./Components/Screen";
import * as ScreenActions from "./Actions/Actions"

const DATA = 
  [
    {
      text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
      name: "My name",
      img: "img/avatar-1.jpg",
      title: "CEO, CompanyName"
    },
    {
      text: "A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.",
      name: "My name",
      img: "img/avatar-2.jpg",
      title: "Manager, Google"
    },
    {
      text: "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.",
      name: "My name",
      img: "img/avatar-3.jpg",
      title: "PR, Name"
    },
    {
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. At temporibus tempora necessitatibus reiciendis provident deserunt maxime sit id. Dicta aut voluptatibus placeat quibusdam vel, dolore.",
      name: "My name",
      img: "img/avatar-4.jpg",
      title: "CEO, CompanyName4"
    },
    {
      text: "Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life.",
      name: "My name",
      img: "img/avatar-5.jpg",
      title: "Manager, CompanyName"
    },
    {
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. At temporibus tempora necessitatibus reiciendis provident deserunt maxime sit id. Dicta aut voluptatibus placeat quibusdam vel, dolore.",
      name: "My name",
      img: "img/avatar-6.jpg",
      title: "CEO, Company"
    }
  ];

class App extends Component {
  render() {
    return (
      <div className="App">
        <Carousel comments={DATA.slice(0, 3)} {...bindActionCreators(ScreenActions, store.dispatch)}/>
        <Screen comments={DATA} {...this.props} {...bindActionCreators(ScreenActions, store.dispatch)}/>      
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    visible: state.visible
  }
}

export default connect(mapStateToProps)(App)