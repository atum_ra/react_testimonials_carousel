import { SEE_ALL, CLOSE } from '../Actions/constants'

const
   initialState = {
       visible: false
   };

export default function screenEvent(state = initialState, action) {
   switch (action.type) {

       case SEE_ALL :
           return { ...state,  visible: action.payload };

       case CLOSE :
           return  { ...state, visible: action.payload };

       default :
           return state;
   }
}