import React from "react";
import PropTypes from "prop-types";
import CSSTransitionGroup from "react-addons-css-transition-group";
import "./styles.css";

export default class SliderStage extends React.Component {
    static contextTypes = {
      currentIndex: PropTypes.number.isRequired,
      registerCount: PropTypes.func.isRequired
    };
  
    componentDidMount() {
      this.context.registerCount(
        React.Children.count(this.props.children)
      );
    }
  
    render() {
      const style = { ...this.props.style};
  
      return (
        <div {...this.props} style={style}>
          <CSSTransitionGroup component="div" className="animated-list"
            transitionName="fade"
            transitionEnterTimeout={600}
            transitionLeaveTimeout={600}
          >
            {React.cloneElement(
              this.props.children[this.context.currentIndex],
              {
                key: this.context.currentIndex
              }
            )}
          </CSSTransitionGroup>
        </div>
      );
    }
  }