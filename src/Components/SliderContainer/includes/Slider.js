import React from "react";
import PropTypes from "prop-types";

export default class Slider extends React.Component {
    static propTypes = {
      initialIndex: PropTypes.number.isRequired,
      autoplay: PropTypes.bool,
      onTogglePlay: PropTypes.func,
      duration: PropTypes.number
    };
  
    static defaultProps = {
      autoplay: false,
      duration: 2500,
      initialIndex: 0
    };
  
    static childContextTypes = {
      currentIndex: PropTypes.number,
      next: PropTypes.func,
      prev: PropTypes.func,
      registerCount: PropTypes.func,
      toggleAutoPlay: PropTypes.func
    };
  
    state = {
      currentIndex: this.props.initialIndex || 0
    };
  
    slideCount = null;
    interval = null;
  
    getChildContext() {
      return {
        currentIndex: this.state.currentIndex,
        next: this.next,
        prev: this.prev,
        registerCount: count => (this.slideCount = count),
        toggleAutoPlay: () => this.toggleAutoPlay()
      };
    }
  
    componentDidMount() {
      if (this.props.autoPlay) this.startAutoPlay();
    }
  
    toggleAutoPlay() {
      if (this.interval) this.stopAutoPlay();
      else this.startAutoPlay();
  
      this.props.onTogglePlay(!!this.interval);
    }
  
    startAutoPlay() {
      this.interval = setInterval(this.next, this.props.duration);
    }
  
    stopAutoPlay() {
      clearInterval(this.interval);
      this.interval = null;
    }
  
    prev = () => {
      let { currentIndex } = this.state;
  
      currentIndex--;
  
      if (currentIndex < 0) currentIndex = this.slideCount - 1;
  
      this.setState({ currentIndex });
    };
  
    next = () => {
      let { currentIndex } = this.state;
  
      currentIndex++;
  
      if (currentIndex === this.slideCount) currentIndex = 0;
  
      this.setState({ currentIndex });
    };
  
    render() {
      const {
        initialIndex,
        autoplay,
        onTogglePlay,
        duration,
        ...props
      } = this.props;
      return <div {...props} />;
    }
  }