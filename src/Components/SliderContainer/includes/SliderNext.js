import React from "react";
import PropTypes from "prop-types";

export default class SliderNext extends React.Component {
    static contextTypes = {
      next: PropTypes.func.isRequired
    };
  
    render() {
      return <button {...this.props} onClick={this.context.next} />;
    }
  }