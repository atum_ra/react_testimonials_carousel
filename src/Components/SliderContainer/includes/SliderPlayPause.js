import React from "react";
import PropTypes from "prop-types";

export default class SliderPlayPause extends React.Component {
    static contextTypes = {
      toggleAutoPlay: PropTypes.func.isRequired
    };
  
    render() {
      return (
        <button {...this.props} onClick={this.context.toggleAutoPlay} />
      );
    }
  }