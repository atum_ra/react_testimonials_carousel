import React from "react";
import PropTypes from "prop-types";
import SliderStage from "./includes/SliderStage"
import Slider from "./includes/Slider"
import Slide from"./Slide"


export default class SliderContainer extends React.Component {

  static propTypes = {
    comments: PropTypes.array
  };

  state = {
    isPlaying: true
  };
  
  render() {
    const comments = this.props.comments;

    return (
      <div className="content">
       

        <Slider
          initialIndex={0}
          duration={3000}
          autoPlay={this.state.isPlaying}
        >
          <SliderStage >
            {comments.map(Slide)}   
           </SliderStage>
        </Slider>
      </div>
    );
  }
}

