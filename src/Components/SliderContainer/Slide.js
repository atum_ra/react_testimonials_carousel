import React from "react";

export default function Slide(comment, i) {
  
    return (
     <div key={i} className="cd-testimonials">
         <p>{comment.text}</p>
         <div className="cd-author">
             <img src={comment.img} alt="" />
             <ul className="cd-author-info">
                 <li>{comment.name}</li>
                 <li>{comment.title}</li>
             </ul>
         </div>
     </div>
  );
}