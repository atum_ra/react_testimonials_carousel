import React from "react";
import Masonry from 'react-masonry-css'
import PropTypes from "prop-types";
import Slide from"./SliderContainer/Slide"
import "./masonry-style.css";

export default class Screen extends React.Component {
    static propTypes = {
        comments: PropTypes.array
    };

    state = {
        visible: this.props.visible
    }

    componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        if (nextProps.visible !== this.state.visible) {
            this.setState({ visible: nextProps.visible });
        }
    }

    componentDidMount() {
        this.node.focus();
    }

    componentDidUpdate() {
        this.node.focus();
    }

    onCloseClick = e => {
        this.props.ScreenHide(false)
    }

    handleKeyDown = e => {
        if (e.keyCode === 27) {
            this.props.ScreenHide(false)
        }
    }

    render() {

        const comments = this.props.comments;

        return (
            <div className={'cd-testimonials-all ' + (this.state.visible ? 'is-visible' : '')} 
            tabIndex="0" 
            onKeyDown={this.handleKeyDown} 
            ref={node => (this.node = node)} 
            >
                <div className="cd-testimonials-all-wrapper" >
                    <Masonry
                        breakpointCols={{ default: 3, 800: 2 }}
                        className="my-masonry-grid"
                        columnClassName="my-masonry-grid_column">
                        {comments.map(Slide)}
                    </Masonry>
                </div>
                <a className="close-btn" onClick={this.onCloseClick}>Close</a>
            </div>
        );
    }
}

