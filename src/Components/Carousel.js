import React from "react";
import SliderContainer from "./SliderContainer/SliderContainer";
import PropTypes from "prop-types";

export default class Carousel extends React.Component {
  static propTypes = {
    comments: PropTypes.array
   };

  onSeeAllClick = e => {
    this.props.ScreenShow(true)
  }

  render() {
   
    return (
      
      <div className="cd-testimonials-wrapper cd-container">
      
        <SliderContainer {...this.props}/>

       <a className="cd-see-all" onClick={this.onSeeAllClick}>See all</a>
      </div>  
      
    );
  }
}

