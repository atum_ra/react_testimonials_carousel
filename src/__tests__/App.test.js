import 'jsdom-global/register'; 

import React from 'react';

import {Provider} from "react-redux"
import { shallow, mount } from 'enzyme';

import configureStore from 'redux-mock-store'

import App from '../App';

describe('It tests main functions of Component <App />', () => {

  const initialState = {
    visible: false
  };

  const mockStore = configureStore();
  let store;

  store = mockStore(initialState)
  
  it('renders without crashing', () => {

    const wrapper = mount(<Provider store={store}>
      <App />
    </Provider>);
  });
  
  it('contains 3 slides in carusel', () => {
   const wrapper = mount(<Provider store={store}>
      <App />
    </Provider>);

    expect(wrapper.find('.animated-list').length).toBe(3);
  });

  it('contains 6 testimonials in masonry-grid', () => {
   const wrapper = mount(<Provider store={store}>
      <App />
    </Provider>);
   
    expect(wrapper.find('.my-masonry-grid').find('.cd-testimonials').length).toBe(6);
  });

  it('simulate button click See ALL and changed state of Screen with testimonials is visible', () => {
    const wrapper = mount(<Provider store={store}>
       <App />
     </Provider>);
     wrapper.find('.cd-see-all').simulate('click');

     expect(wrapper.exists('.cd-testimonials-all is-visible')).toBe(true)
   });

});
