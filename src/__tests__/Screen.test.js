import 'jsdom-global/register'; 

import React from 'react';

import {Provider} from "react-redux"
import { shallow, mount } from 'enzyme';

import configureStore from 'redux-mock-store'

import Screen from '../Components/Screen';

describe('It tests hide/show of Screen with all testimonials', () => {

    const initialState = {
      visible: false
    };

    const mockStore = configureStore();
    let store;
  
    store = mockStore(initialState)

    const DATA =
        [
            {
                text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
                name: "My name",
                img: "img/avatar-1.jpg",
                title: "CEO, CompanyName"
            }
        ];

    it('changes state of Screen to show', () => {
      const wrapper = mount(<Provider store={store}>
        <Screen comments={DATA} />
      </Provider>);
      wrapper.setState({visible:true})
   

      expect(wrapper.exists('.cd-testimonials-all is-visible')).toBe(true)
    });

    it('after state changed via SEE_ALL Screen got state visible', () => {
      const wrapper = mount(<Provider store={store}>
        <Screen comments={DATA} />
      </Provider>);
      wrapper.setState({visible:true})
      expect(wrapper.state('visible')).toEqual(true);

    });

});